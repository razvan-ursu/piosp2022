package org.sample;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.sample.models.Order;
import org.sample.repository.ArrayListBasedRepository;
import org.sample.repository.ConcurrentHashMapBasedRepository;
import org.sample.repository.InMemoryRepository;
import org.sample.repository.TreeSetBasedRepository;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class RepositoryBenchmark {
    private static final List<Order> data = Arrays.asList(
            new Order(1, 1, 1),
            new Order(2, 2, 2),
            new Order(3, 3, 3),
            new Order(4, 4, 4),
            new Order(5, 5, 5)
    );

    @State(Scope.Benchmark)
    public static class ArrayListRepositoryState {
        InMemoryRepository<Order> repo = new ArrayListBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapRepositoryState {
        InMemoryRepository<Order> repo = new ConcurrentHashMapBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class TreeSetRepositoryState {
        InMemoryRepository<Order> repo = new TreeSetBasedRepository<>();
    }

    @Benchmark
    public void add_ArrayList(ArrayListRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        data.forEach((item) -> state.repo.remove(item));
    }

    @Benchmark
    public void add_ConcurrentHashMap(ConcurrentHashMapRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        data.forEach((item) -> state.repo.remove(item));
    }

    @Benchmark
    public void add_TreeSet(TreeSetRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        data.forEach((item) -> state.repo.remove(item));
    }

    @Benchmark
    public void remove_ArrayList(ArrayListRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        Order item = data.get(2);
        state.repo.remove(item);
    }

    @Benchmark
    public void remove_ConcurrentHashMap(ConcurrentHashMapRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        Order item = data.get(2);
        state.repo.remove(item);
    }

    @Benchmark
    public void remove_TreeSet(TreeSetRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        Order item = data.get(2);
        state.repo.remove(item);
    }

    @Benchmark
    public void contains_ArrayList(ArrayListRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        Order item = data.get(2);
        state.repo.contains(item);
    }

    @Benchmark
    public void contains_ConcurrentHashMap(ConcurrentHashMapRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        Order item = data.get(2);
        state.repo.contains(item);
    }

    @Benchmark
    public void contains_TreeSet(TreeSetRepositoryState state) {
        data.forEach((item) -> state.repo.add(item));
        Order item = data.get(2);
        state.repo.contains(item);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(RepositoryBenchmark.class.getSimpleName())
//                .addProfiler(HotspotMemoryProfiler.class)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
