package org.sample.repository;


import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {

    private static long currentIndex = 0L;

    private final Map<Long, T> map;

    public ConcurrentHashMapBasedRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T item) {
        map.put(currentIndex++, item);
    }

    @Override
    public boolean contains(T item) {
        return map.containsValue(item);
    }

    @Override
    public void remove(T item) {
        Optional<Map.Entry<Long, T>> optional = map
                .entrySet()
                .stream()
                .filter((it) -> it.getValue().equals(item))
                .findFirst();
        if (optional.isPresent()) {
            Map.Entry<Long, T> pair = optional.get();
            map.remove(pair.getKey());
        }
    }
}
