package org.sample.repository;

import org.eclipse.collections.impl.set.sorted.mutable.TreeSortedSet;

import java.util.Set;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {

    private final Set<T> set;

    public TreeSetBasedRepository() {
        this.set = new TreeSortedSet<>();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
