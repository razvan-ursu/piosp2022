package org.sample.repository;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {

    private final List<T> arrayList;

    public ArrayListBasedRepository() {
        this.arrayList = new ObjectArrayList<T>();
    }

    @Override
    public void add(T item) {
        arrayList.add(item);
    }

    @Override
    public boolean contains(T item) {
        return arrayList.contains(item);
    }

    @Override
    public void remove(T item) {
        arrayList.remove(item);
    }
}
