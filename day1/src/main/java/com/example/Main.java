package com.example;

import com.example.models.calculator.MyCalculator;

/**
 * This is a class.
 */
public class Main {

    public static void main(String[] args) {
        MyCalculator calculator = new MyCalculator();
        try {
            System.out.println("Result: " + calculator.execute("1 + 1"));
            System.out.println("Result: " + calculator.execute("9 - 20"));
            System.out.println("Result: " + calculator.execute("3 * 3"));
            System.out.println("Result: " + calculator.execute("100 / 2"));
            System.out.println("Result: " + calculator.execute("min(5,3)"));
            System.out.println("Result: " + calculator.execute("max(5,3)"));
            System.out.println("Result: " + calculator.execute("sqrt(25)"));

            System.out.println("=========================================");
//            System.out.println("Result: " + calculator.execute("10 / 0"));
            System.out.println("Result: " + calculator.execute("max(3)"));
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
