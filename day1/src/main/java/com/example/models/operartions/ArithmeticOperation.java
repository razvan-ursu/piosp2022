package com.example.models.operartions;

public abstract class ArithmeticOperation<T extends Number> implements Operation<T> {
    public abstract T execute(T number1, T number2);
}
