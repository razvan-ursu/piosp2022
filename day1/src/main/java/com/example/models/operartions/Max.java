package com.example.models.operartions;

public class Max extends ArithmeticOperation<Float> {

    @Override
    public Float execute(Float number1, Float number2) {
        if (number1 > number2) {
            return number1;
        } else {
            return number2;
        }
    }
}
