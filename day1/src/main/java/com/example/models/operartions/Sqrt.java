package com.example.models.operartions;

public class Sqrt implements Operation<Float> {
    public static final Float PRECISION = 0.001f;

    public Float execute(Float number) {
        for (int i = 1; i < number; ++i) {
            int p = i * i;
            if (p == number) {
                // perfect square
                return (float) i;
            }
            if (p > number) {
                // found left part of decimal
                return sqrt(number, (float) (i - 1), (float) i);
            }
        }
        return Float.NaN;
    }

    private static Float sqrt(Float num, Float low, Float high) {
        Float mid = (low + high) / 2;
        Float p = mid * mid;

        if (p.equals(num) || (Math.abs(num - p) < PRECISION)) {
            return mid;
        }
        if (p < num) {
            return sqrt(num, mid, high);
        }
        return sqrt(num, low, mid);
    }
}
