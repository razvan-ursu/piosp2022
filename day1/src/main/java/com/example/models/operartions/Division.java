package com.example.models.operartions;

import com.example.exceptions.ArithmeticException;

public class Division extends ArithmeticOperation<Float> {

    @Override
    public Float execute(Float number1, Float number2) {
        if (number2 == 0) {
            throw new ArithmeticException("Division by 0 not permitted!");
        }
        return number1 / number2;
    }
}
