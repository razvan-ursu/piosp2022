package com.example.models.operartions;

public class Multiplication extends ArithmeticOperation<Float>{

    @Override
    public Float execute(Float number1, Float number2) {
        return number1 * number2;
    }
}
