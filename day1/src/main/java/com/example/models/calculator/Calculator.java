package com.example.models.calculator;

public interface Calculator<T extends Number> {
    T execute(String expression);
}
