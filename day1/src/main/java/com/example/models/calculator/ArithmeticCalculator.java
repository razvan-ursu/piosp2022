package com.example.models.calculator;

import com.example.exceptions.CalculatorException;
import com.example.models.operartions.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ArithmeticCalculator implements Calculator<Float> {
    private final Map<Character, ArithmeticOperation<Float>> arithmeticOperationsMap = new HashMap<>();

    public ArithmeticCalculator() {
        arithmeticOperationsMap.put('+', new Addition());
        arithmeticOperationsMap.put('-', new Subtraction());
        arithmeticOperationsMap.put('*', new Multiplication());
        arithmeticOperationsMap.put('/', new Division());
    }

    @Override
    public Float execute(String expression) {
        var parsedExpression = parseExpression(expression);
        var num1 = Float.valueOf(parsedExpression.get(0));
        var operationSign = parsedExpression.get(1).charAt(0);
        var num2 = Float.valueOf(parsedExpression.get(2));

        ArithmeticOperation<Float> operation = arithmeticOperationsMap.get(operationSign);
        return operation.execute(num1, num2);
    }


    private List<String> parseExpression(String expression) {
        var parsed = Arrays.stream(expression.split(" ")).collect(Collectors.toList());
        if (parsed.size() != 3) {
            throw new CalculatorException("Invalid expression!");
        }
        return parsed;
    }
}
