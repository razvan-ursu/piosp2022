package com.example.models.calculator;

import com.example.exceptions.ArithmeticException;
import com.example.exceptions.CalculatorException;

public class MyCalculator {
    private final ArithmeticCalculator arithmeticCalculator = new ArithmeticCalculator();
    private final FunctionCalculator functionCalculator = new FunctionCalculator();

    public Float execute(String expression) throws RuntimeException {
        try {
            return arithmeticCalculator.execute(expression);

        } catch (ArithmeticException ae) {
            System.out.println(ae.getMessage());
        } catch (Exception ex) {
            return functionCalculator.execute(expression);
        }
        throw new CalculatorException("Expression could not be processed!");
    }
}
