package com.example.models.calculator;

import com.example.exceptions.CalculatorException;
import com.example.models.operartions.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FunctionCalculator implements Calculator<Float> {
    private final Map<String, Operation<Float>> operationMap = new HashMap<>();

    public FunctionCalculator() {
        operationMap.put("min", new Min());
        operationMap.put("max", new Max());
        operationMap.put("sqrt", new Sqrt());
    }

    @Override
    public Float execute(String expression) {
        var parsedExpression = parseExpression(expression);

        var operation = operationMap.get(parsedExpression.get(0));
        Float num1 = Float.valueOf(parsedExpression.get(1));
        Float num2 = null;
        if (parsedExpression.size() > 2) {
            num2 = Float.valueOf(parsedExpression.get(2));
        }

        if (num2 != null) {
            return ((ArithmeticOperation<Float>) operation).execute(num1, num2);
        } else if (operation instanceof Sqrt) {
            return ((Sqrt) operation).execute(num1);
        } else {
            throw new CalculatorException("Expression could not be executed!");
        }
    }

    private List<String> parseExpression(String expression) {
        // expr = "min(5,9)"
        var p1 = expression.replaceAll("\\(", " ");
        var p2 = p1.replaceAll("\\)", " ");
        var p3 = p2.replaceAll(",", " ");
        var parse = p3.split(" ");
        return Arrays.stream(parse).collect(Collectors.toList());
    }
}
