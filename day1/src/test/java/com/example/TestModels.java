package com.example;

import com.example.models.operartions.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class TestModels {

    private Addition addition;
    private Subtraction subtraction;
    private Multiplication multiplication;
    private Division division;
    private Min min;
    private Max max;
    private Sqrt sqrt;

    @Before
    public void setup() {
        addition = new Addition();
        subtraction = new Subtraction();
        multiplication = new Multiplication();
        division = new Division();
        min = new Min();
        max = new Max();
        sqrt = new Sqrt();
    }

    @Test
    public void executeShouldReturnExpectedValue() {
        assert (addition.execute(2f, 2f) == 4f);
        assert (subtraction.execute(5f, 2f) == 3f);
        assert (multiplication.execute(3f, 3f) == 9f);
        assert (division.execute(10f, 2f) == 5f);
        assert (min.execute(10f, 2f) == 2f);
        assert (max.execute(10f, 2f) == 10f);
        assert (sqrt.execute(25f) == 5f);
    }

    @Test
    public void divisionByZeroShouldReturnError() {
        assertThrows(RuntimeException.class, () -> division.execute(10f, 0f));
        assertThrows(RuntimeException.class, () -> division.execute(50f, 0f));
        assertThrows(RuntimeException.class, () -> division.execute(20f, 0f));
        assertThrows(RuntimeException.class, () -> division.execute(25f, 0f));
    }
}